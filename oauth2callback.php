<?php
//taken from https://developers.google.com/api-client-library/php/auth/web-app
require __DIR__ . '/vendor/autoload.php';

session_start();

$client = new Google_Client();
$client->setAuthConfigFile('client_secrets.json');
$client->setRedirectUri('http://dvorama.myweb.jce.ac.il/calendar/oauth2callback.php');
$client->addScope('https://www.googleapis.com/auth/calendar');

if (! isset($_GET['code'])) {
  $auth_url = $client->createAuthUrl();
  header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
} else {
  $client->authenticate($_GET['code']);
  $_SESSION['access_token'] = $client->getAccessToken();
  $redirect_uri = 'http://dvorama.myweb.jce.ac.il/calendar';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}