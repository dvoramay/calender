<?php

require __DIR__ . '/vendor/autoload.php';

session_start();
if(isset($_POST["submit"])){
$client = new Google_Client();
$client->setAuthConfigFile('client_secrets.json');
$client->addScope('https://www.googleapis.com/auth/calendar');

if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
  
  $client->setAccessToken($_SESSION['access_token']);
  $service = new Google_Service_Calendar($client);

  $event = new Google_Service_Calendar_Event(array(
  'summary' => 'dvoratest',
  'location' => '‪Jerusalem‬',
  'description' => 'Testing the google calendar API',
  'start' => array(
    'dateTime' => $_POST['start'].':00+02:00',
    'timeZone' => 'America/Los_Angeles',
  ),
  'end' => array(
    'dateTime' =>  $_POST['end'].':00+02:00',
    'timeZone' => 'America/Los_Angeles',
  ),
  'recurrence' => array(
    'RRULE:FREQ=DAILY;COUNT=1'
  ),
  'participants' => array(
    array('email' => 'eden963852@example.com'),
    array('email' => 'shiranach0@exampl.com'),
  ),
  'attendees' => array(
    array('email' => 'eden963852@example.com'),
    array('email' => 'shiranach0@example.com'),
	array('email' => 'ta@example.com'),
  ),
  'reminders' => array(
    'useDefault' => FALSE,
    'overrides' => array(
      array('method' => 'email', 'minutes' => 24 * 60),
      array('method' => 'popup', 'minutes' => 10),
    ),
  ),
));
  
$calendarId = 'primary';
$event = $service->events->insert($calendarId, $event);
printf('Event created: %s\n', $event->htmlLink);
  
} else {
  $redirect_uri =  'http://dvorama.myweb.jce.ac.il/calendar/oauth2callback.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}
}
?> 
<html> 
<head> 
<title>insert data to calendar</title> 
<link rel="stylesheet" type="text/css" href="style.css"> 
</head> 
<body> 

<div id="main"> 
<h1>Insert data into database using firebase</h1> 
<div id="login"> 
<h2>Calendar Form</h2> 
<hr/> 
<form action="" method="post"> 
<label>Date and Time start:</label> 
<input type="dateTime-local" name="start" id="date"  placeholder="Please Enter date" required/><br /><br /> 
<label>Date and Time end:</label> 
<input type="dateTime-local" name="end" id="date"  placeholder="Please Enter date" required/><br /><br /> 
<input type="submit" value=" Submit " name="submit"/><br /> 
</form> 
</div> 


</div> 

</body> 
</html> 

